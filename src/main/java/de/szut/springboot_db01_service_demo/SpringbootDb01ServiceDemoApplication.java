package de.szut.springboot_db01_service_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDb01ServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDb01ServiceDemoApplication.class, args);
    }

}
