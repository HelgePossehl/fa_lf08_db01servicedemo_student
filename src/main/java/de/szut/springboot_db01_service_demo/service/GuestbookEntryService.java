package de.szut.springboot_db01_service_demo.service;

import de.szut.springboot_db01_service_demo.model.GuestbookEntry;
import de.szut.springboot_db01_service_demo.repository.GuestbookEntryRepository;
import de.szut.springboot_db01_service_demo.request.GuestbookEntryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuestbookEntryService {

    @Autowired
    private GuestbookEntryRepository repository;

    /**
     * Erstellt einen neuen GuestbookEntry.
     * @param request   Das Request-Objekt.
     * @return  Der GuestbookEntry.
     */
    public GuestbookEntry create(GuestbookEntry request) {
        return repository.save(request);
    }

    /**
     * Liest einen GuestbookEntry auf Basis seiner ID.
     * @param id    Die ID.
     * @return  Der GuestbookEntry.
     */
    public GuestbookEntry readById(long id) {
        return repository.findById(id).get();
    }

    /**
     * Liest alle GuestbookEntries.
     * Die Datensätze werden der ID nach aufsteigend sortiert.
     * Inhaltlich werden die zuerst hinzugefügten GuestbookEntries auch als erstes angezeigt.
     * @return  Die Liste aller GuestbookEntries.
     */
    public List<GuestbookEntry> readAll() {
        return repository.findAll();
    }

    /**
     * Liest alle GuestbookEntries.
     * Die Datensätze werden der ID nach absteigend sortiert.
     * Inhaltlich werden die zuletzt hinzugefügten GuestbookEntries als erstes angezeigt.
     * @return  Die Liste aller GuestbookEntries.
     */
    public List<GuestbookEntry> readAllOrderedByIdDesc() {
        return repository.findAllByOrderByIdDesc();
    }

    /**
     * Aktualisiert einen GuestbookEntry.
     * @param guestbookEntry   Das Request-Objekt.
     * @return  Der GuestbookEntry.
     */
    public GuestbookEntry update(GuestbookEntry guestbookEntry){
        GuestbookEntry serializedGuestbookEntry = readById(guestbookEntry.getId());
        serializedGuestbookEntry.setTitle(guestbookEntry.getTitle());
        serializedGuestbookEntry.setComment(guestbookEntry.getComment());
        serializedGuestbookEntry.setCommenter(guestbookEntry.getCommenter());
        return repository.save(serializedGuestbookEntry);
    }

    /**
     * Löscht einen Eintrag auf Basis seiner ID.
     * @param id    Die ID.
     */
    public void delete(long id) {
        repository.deleteById(id);
    }

}
