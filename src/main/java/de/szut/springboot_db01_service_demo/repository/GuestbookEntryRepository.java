package de.szut.springboot_db01_service_demo.repository;

import de.szut.springboot_db01_service_demo.model.GuestbookEntry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// Zugriff auf die Datenbank über http://localhost:8080/h2-console.
// JDBC-URL anpassen gemäß application.properties
public interface GuestbookEntryRepository extends JpaRepository<GuestbookEntry, Long> {

    public List<GuestbookEntry> findAllByOrderByIdDesc();

    public List<GuestbookEntry> findAllByOrderByIdAsc();
}
